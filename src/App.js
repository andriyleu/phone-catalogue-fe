import React from 'react';
import './App.css';
import PhoneListComponent from './components/PhoneList';
import Container from '@material-ui/core/Container';
import AppHeaderComponent from './components/AppHeader';
import { parsePhones } from './actions/actions'
import { connect } from 'react-redux';
import CircularProgress from '@material-ui/core/CircularProgress'

import {
  BrowserRouter as Router,
  Switch,
  Route
} from "react-router-dom";
import DetailPage from './components/DetailPage';

class App extends React.Component {

  state = {
    loading: false
  };

  // Fetch data from server and save it in redux store
  componentDidMount() {
    this.setState({ loading: true });
    setTimeout(() => {
      this.props.parsePhones()
        .then(() => this.setState({ loading: false }))
        .catch(() => this.setState({ loading: false }));
    }, 1000);
  }

  render() {
    return (<div className="App">
      <Router>
        <div>
          <AppHeaderComponent></AppHeaderComponent>
          <Container maxWidth="md">
            {this.state.loading && <CircularProgress className="spinner" />}
            <Switch>
              <Route exact path="/">
                <PhoneListComponent></PhoneListComponent>
              </Route>
              <Route path="/detail/:id">
                <DetailPage></DetailPage>
                <Route />
              </Route>
            </Switch>
          </Container>
        </div>
      </Router>
    </div>
    );
  }
}

const mapStateToProps = (state) => {
  return {
    phones: state.phones
  }
}

const mapDispatchToProps = { parsePhones }

export default connect(
  mapStateToProps,
  { parsePhones }
)(App);

