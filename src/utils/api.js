import axios from 'axios';

export const API_ENDPOINT = 'http://localhost:3000/';

export function getPhones() {
    return axios.get(API_ENDPOINT.concat('phones'));
}