import React from 'react';
import Card from '@material-ui/core/Card';
import CardContent from '@material-ui/core/CardContent';
import CardMedia from '@material-ui/core/CardMedia';
import Typography from '@material-ui/core/Typography';
import CardActionArea from '@material-ui/core/CardActionArea';
import './Item.css';
import { withRouter } from 'react-router'
import { API_ENDPOINT } from '../utils/api';

class ItemComponent extends React.Component {

    constructor(props) {
        super(props);
    }

    navigate() {
        this.props.history.push('/detail/'.concat(this.props.phone.id), {
            phone: this.props.phone
        })
    }

    render() {
        const { phone } = this.props;
        return (
            <Card key={phone.id} onClick={this.navigate.bind(this)}>
                <CardActionArea className="phone-item">
                    <CardMedia className="phone-image"
                        component="img"
                        alt={phone.name}
                        src={API_ENDPOINT.concat('images/').concat(phone.imageFileName)}
                        title={phone.name}
                    />
                    <CardContent className="phone-info">
                        <Typography align="left" gutterBottom variant="h5" component="h2">
                            {phone.name}
                        </Typography>
                        <Typography variant="body2" align="left" color="textSecondary" component="p" className="phone-description">
                            {phone.description}
                        </Typography>
                    </CardContent>
                </CardActionArea>
            </Card>
        );
    }
}

export default withRouter(ItemComponent);
