import { getPhones } from '../utils/api';
/**
 * TYPE OF ACTIONS
 */
export const PARSE_PHONES = 'PARSE_PHONES';

/**
 * ACTION CREATOR
 */
export function parsePhones() {
    return async (dispatch, getState) => {
        let res = await getPhones();
        return dispatch({ type: PARSE_PHONES, phones: res.data });
    }
}