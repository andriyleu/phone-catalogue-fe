import React from 'react';
import Typography from '@material-ui/core/Typography';
import AppBar from '@material-ui/core/AppBar';
import Toolbar from '@material-ui/core/Toolbar';
import Container from '@material-ui/core/Container';
import ArrowBack from '@material-ui/icons/ArrowBack';
import IconButton from '@material-ui/core/IconButton';
import { withRouter } from 'react-router-dom';


class AppHeaderComponent extends React.Component {

    handleBack = () => {
        this.props.history.push('/');
    }

    render() {
        return (
            <AppBar position="sticky" color="primary">
                <Container maxWidth="md">
                    <Toolbar>
                        {
                            this.props.history.location.pathname !== '/' && (
                                <IconButton onClick={this.handleBack} edge="start" color="inherit" aria-label="menu">
                                    <ArrowBack />
                                </IconButton>)
                        }
                        <Typography variant="h6" color="inherit">
                            Phone Catalogue
                        </Typography>
                    </Toolbar>
                </Container>
            </AppBar>
        );
    }
}

export default withRouter(AppHeaderComponent);
