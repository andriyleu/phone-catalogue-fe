import { PARSE_PHONES } from '../actions/actions';

const initialState = {
    phones: []
};

function phoneList(state = initialState, action) {
    switch (action.type) {
        case PARSE_PHONES:
            return Object.assign({}, state, { phones: action.phones })
        default:
            return state
    }
}

export default phoneList