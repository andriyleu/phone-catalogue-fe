import React from 'react';
import './DetailPage.css';
import Typography from '@material-ui/core/Typography';
import Chip from '@material-ui/core/Chip';
import Avatar from '@material-ui/core/Avatar';
import EuroIcon from '@material-ui/icons/Euro';
import HeightIcon from '@material-ui/icons/Height';
import BuildIcon from '@material-ui/icons/Build';
import Grid from '@material-ui/core/Grid';
import Paper from '@material-ui/core/Paper';
import Breadcrumbs from '@material-ui/core/Breadcrumbs';
import CardMedia from '@material-ui/core/CardMedia';
import { withRouter } from 'react-router-dom';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';
import { API_ENDPOINT } from '../utils/api';

class DetailPage extends React.Component {



    constructor(props) {
        super(props);
    }

    render() {

        const phone = this.props.phones[this.props.match.params.id];

        if (!phone) {
            return null;
        }

        return (
            <div className="detail-page">
                <Grid container spacing={3}>
                    <Grid item xs={12} md={6}>
                        <Paper><CardMedia
                            component="img"
                            src={API_ENDPOINT.concat('images/').concat(phone.imageFileName)}
                        />
                        </Paper>
                    </Grid>
                    <Grid item xs={12} md={6}>
                        <Paper className="details">
                            <Breadcrumbs aria-label="breadcrumb">
                                <Typography variant="h6" color="textSecondary">{phone.manufacturer}</Typography>
                                <Typography variant="h6" color="textPrimary">{phone.name}</Typography>
                            </Breadcrumbs>
                            <Typography className="desc" align="left" variant="body2" color="inherit">
                                {phone.description}
                            </Typography>
                            <div className="detail-badge">
                                <Chip
                                    icon={<EuroIcon />}
                                    label={phone.price}
                                    variant="outlined"
                                    className="badge"
                                />
                                <Chip
                                    icon={<HeightIcon />}
                                    label={phone.screen}
                                    variant="outlined"
                                    className="badge"
                                />
                                <Chip
                                    icon={<BuildIcon />}
                                    label={phone.processor}
                                    variant="outlined"
                                    className="badge"
                                />
                                <Chip
                                    avatar={<Avatar style={{ backgroundColor: phone.color }}> </Avatar>}
                                    label="Color"
                                    variant="outlined"
                                    className="badge"
                                />
                            </div>
                        </Paper>
                    </Grid>
                </Grid>
            </div>
        );
    }
}

DetailPage.propTypes = {
    phones: PropTypes.array.isRequired
};

const mapStateToProps = state => {
    return {
        phones: state.phones
    };
};

export default connect(mapStateToProps)(withRouter(DetailPage));
