import React from 'react';
import ItemComponent from './Item';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';

class PhoneListComponent extends React.Component {

    constructor(props) {
        super(props);
    }

    render() {
        return (
            <div className="phone-list-page">
                {this.props.phones.map((phone) =>
                    <ItemComponent key={phone.id} phone={phone}></ItemComponent>
                )}
            </div>
        );
    }
}

PhoneListComponent.propTypes = {
    phones: PropTypes.array.isRequired
};

const mapStateToProps = state => {
    return {
        phones: state.phones
    };
};

export default connect(mapStateToProps)(PhoneListComponent);
